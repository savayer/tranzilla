'use strict';

$(function () {
    Revealator.effects_padding = '-200';

    $('.slider').slick({
        infinite: true,
        slideToShow: 1,
        slideToScroll: 1,
        dots: true,
        fade: true,
        speed: 1000,
        prevArrow: '<i class="material-icons slider-arrow prev-arrow">chevron_left</i>',
        nextArrow: '<i class="material-icons slider-arrow next-arrow">chevron_right</i>',
        responsive: [{
            breakpoint: 920,
            settings: {
                cssEase: ''
            }
        }]
    });

    setTimeout(function () {
        $('.show-immediately.revealator-slideup').addClass('revealator-within');
    }, 300);

    $('.clients').slick({
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: false,
        responsive: [{
            breakpoint: 1124,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">chevron_left</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">chevron_right</i>'
            }
        }, {
            breakpoint: 920,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">chevron_left</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">chevron_right</i>'
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">chevron_left</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">chevron_right</i>'
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                arrows: true,
                prevArrow: '<i class="material-icons clients-arrow clients-prev-arrow">chevron_left</i>',
                nextArrow: '<i class="material-icons clients-arrow clients-next-arrow">chevron_right</i>'
            }
        }]
    });

    $('.menu-toggle').on('click', function () {
        $('.menu-mobile-container').slideToggle();
        $('.overlay').toggleClass('active');
        if ($(this).text() == 'close') {
            $(this).text('menu');
            $('body').css('overflow', 'unset');
        } else {
            $(this).text('close');
            $('body').css('overflow', 'hidden');
        }
    });

    $('.menu-mobile__li .dropdown').on('click', function () {
        var iconArrow = $(this).children();
        if (iconArrow.text() == 'expand_more') {
            iconArrow.text('expand_less');
        } else {
            iconArrow.text('expand_more');
        }
        $(this).siblings('.dropdown-mobile-menu').slideToggle();
        $(this).closest('.menu-mobile__li').toggleClass('active_li');

        if ($('.menu-mobile__li').hasClass('active_li')) {
            $('.overlay').css('opacity', '.9');
        } else $('.overlay').css('opacity', '.6');
    });

    /*     if ($(window).width() <= 500) {
            $('.revealator-once').removeClass('revealator-slideleft revealator-slideup revealator-zoomin revealator-fade');       
        } */
});